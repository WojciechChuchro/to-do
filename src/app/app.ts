import swaggerUi from 'swagger-ui-express';
import express from "express";
import mongoose from "mongoose";
import cors from "cors";
import router from "./routes";
import dotenv from "dotenv";
import swaggerSpec from './utils/swagger';

dotenv.config();
const app = express();

const PORT: string | number = process.env.PORT || 4000;

app.use(cors());
app.use(express.json());
app.use("/api/v1/", router());
app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

const uri: string = `mongodb://${process.env.MONGO_USERNAME}:${process.env.MONGO_PASSWORD}@localhost:27017`;

mongoose
  .connect(uri)
  .then(() => {
    console.log(`Connection to mongodb successes`);
  })
  .catch((error) => {
    console.log(`Connection to mongodb failed: \n${error}`);
  });

export default app;
