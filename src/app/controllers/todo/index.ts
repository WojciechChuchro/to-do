import { Response, Request } from "express";
import { ITodo } from "../../types/ITodo";
import TodoModel from "../../todoModel";
import Todo from "../../todoModel";
import { HttpStatusCode } from "../../types/enums";

export const getTodos = async (req: Request, res: Response): Promise<void> => {
  try {
    const todos: ITodo[] = await TodoModel.find();
    res.status(HttpStatusCode.Ok).json({ todos: todos });
  } catch (error) {
    res.status(HttpStatusCode.InternalServerError).json({message: "Error occurred while getting todos"})
  }
};

export const createTodo = async (
  req: Request,
  res: Response
): Promise<void> => {
  const { title, desc, isDone } = req.body;
  const newTodo = new Todo({
    title,
    desc,
    isDone,
  });

  try {
    const result = await newTodo.save();
    res.status(HttpStatusCode.Created).json({message: "Todo created", todo: result});
  } catch (error) {
    res.status(HttpStatusCode.InternalServerError).json({message: "Error occurred while creating todo"})
  }
};

export const updateTodo = async (req: Request, res: Response) => {
  const { title, desc, isDone, id } = req.body;
  
  try {
    const todo = await Todo.findById(id);

    if (!todo) {
      res.status(HttpStatusCode.NotFound).json({message: "Todo not found"})
    }

    if (title) todo.title = title;
    if (desc) todo.desc = desc;
    if (isDone) todo.isDone = isDone;

    const updatedTodo = await todo.save();

    res.status(HttpStatusCode.Ok).json({message: "Todo updated", todo: updateTodo})
    } catch (error) {
        res.status(HttpStatusCode.InternalServerError)
  }
};

export const deleteTodo = async (req: Request, res: Response) => {
  const { id } = req.body;

  try {
    const result = await Todo.deleteOne({ _id: id });
    
    if (result.deletedCount === 0) {
      res.status(HttpStatusCode.NotFound).json({message: "Todo not found"})
    }
  } catch (error) {
    res.status(HttpStatusCode.InternalServerError).json({message: "Error occurred while deleting todos"})
  }
};
