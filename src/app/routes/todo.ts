import express from "express";
import {
  createTodo,
  deleteTodo,
  getTodos,
  updateTodo,
} from "../controllers/todo";

export default (router: express.Router) => {
  /**
   * @swagger
   * components:
   *   schemas:
   *     Todo:
   *       type: object
   *       required:
   *         - title
   *         - desc
   *         - isDone
   *       properties:
   *         id:
   *           type: string
   *           description: The auto-generated id of the todo.
   *         title:
   *           type: string
   *           description: The title of the todo task.
   *         desc:
   *           type: string
   *           description: The description of the todo task.
   *         isDone:
   *           type: boolean
   *           description: The completion status of the todo task.
   *         createdAt:
   *           type: string
   *           format: date-time
   *           description: The date and time when the todo was created.
   *         updatedAt:
   *           type: string
   *           format: date-time
   *           description: The date and time when the todo was last updated.
   */

  /**
   * @swagger
   * /todo/:
   *   get:
   *     summary: Returns a list of todos.
   *     responses:
   *       200:
   *         description: A JSON array of todo items.
   *       500:
   *         description: Unexpected Error.
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 $ref: '#/components/schemas/Todo'
   */
  router.get("/todo/", getTodos);
  /**
   * @swagger
   * /todo/:
   *   post:
   *     summary: Creates a new todo.
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             $ref: '#/components/schemas/Todo'
   *     responses:
   *       201:
   *         description: The todo item was successfully created.
   *       500:
   *         description: Unexpected Error.
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Todo'
   */
  router.post("/todo/", createTodo);
  /**
   * @swagger
   * /todo/:
   *   put:
   *     summary: Updates an existing todo.
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             $ref: '#/components/schemas/Todo'
   *     responses:
   *       200:
   *         description: The todo item was successfully updated.
   *       500:
   *         description: Unexpected Error.
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/Todo'
   */
  router.put("/todo/", updateTodo);
  /**
   * @swagger
   * /todo/:
   *   delete:
   *     summary: Deletes a todo.
   *     parameters:
   *       - in: query
   *         name: id
   *         required: true
   *         schema:
   *           type: string
   *         description: The id of the todo to delete.
   *     responses:
   *       200:
   *         description: The todo item was successfully deleted.
   *       500:
   *         description: Unexpected Error.
   */
  router.delete("/todo/", deleteTodo);
};
