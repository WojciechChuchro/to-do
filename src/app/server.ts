import app from "./app";
import swaggerDocs from "./utils/swagger";

const PORT = process.env.PORT || 8080;

app.listen(PORT, () => {
  console.log(`Server started at http://localhost:${PORT}`);
  console.log(`Documentation is available at http://localhost:${PORT}/docs`);
});
