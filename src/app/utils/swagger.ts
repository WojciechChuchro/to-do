import swaggerJSDoc from 'swagger-jsdoc';
import path from 'path';
 const swaggerDefinition = {
  openapi: '3.0.0',
  info: {
    title: 'Express API with Swagger',
    version: '1.0.0',
    description: 'This is a simple CRUD API application made with Express and documented with Swagger',
  },
  servers: [
    {
      url: 'http://localhost:8080',
      description: 'Local server',
    },
  ],
};

const options = {
  swaggerDefinition,
  apis: [path.resolve(__dirname, '../routes/todo.ts')],
};

const swaggerSpec = swaggerJSDoc(options);

export default swaggerSpec;